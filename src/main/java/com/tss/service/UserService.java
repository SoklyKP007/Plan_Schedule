package com.tss.service;

import com.tss.model.User;

import java.util.ArrayList;

/**
 * Created by Torn-Sokly on 5/23/2017.
 */

public interface UserService {
    public ArrayList<User> findUsers();
}
