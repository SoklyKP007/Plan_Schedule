package com.tss.service.impl;

import com.tss.model.User;
import com.tss.repositories.UserRepository;
import com.tss.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by Torn-Sokly on 5/23/2017.
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    public ArrayList<User> findUsers() {
        return userRepository.findUsers();
    }
}
