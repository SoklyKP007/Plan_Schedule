package com.tss.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Created by Torn-Sokly on 5/23/2017.
 */
@Configuration
public class WebInitializer  extends AbstractAnnotationConfigDispatcherServletInitializer{
    protected Class<?>[] getRootConfigClasses() {
        return new java.lang.Class<?>[]{null};
    }

    protected java.lang.Class<?>[] getServletConfigClasses() {
        return new java.lang.Class<?>[]{WebConfig.class};
    }

    protected java.lang.String[] getServletMappings() {
        return new java.lang.String[]{"/"};
    }
}