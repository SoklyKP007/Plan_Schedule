package com.tss.controller;

import com.tss.model.User;
import com.tss.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * Created by Torn-Sokly on 5/23/2017.
 */
@Controller
@RequestMapping("/")
@RestController
public class MainController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/greeting",method = RequestMethod.GET)
    public ArrayList<User> findUsers(){
        return userService.findUsers();
    }
}
