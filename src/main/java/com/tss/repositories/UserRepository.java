package com.tss.repositories;

import com.tss.model.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

/**
 * Created by Torn-Sokly on 5/23/2017.
 */

@Repository
public interface UserRepository {

    public ArrayList<User> findUsers();
}
